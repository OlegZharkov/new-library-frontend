import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {NewBookingComponent} from './new-booking.component';
import {MaterialModule} from '../material-module';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

describe('NewBookingComponent', () => {
  let component: NewBookingComponent;
  let fixture: ComponentFixture<NewBookingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [MaterialModule, BrowserAnimationsModule, FormsModule, RouterModule.forRoot([])],
      declarations: [NewBookingComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewBookingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('input should exist', () => {
    const inputElement = fixture.debugElement.nativeElement.querySelector('input');
    expect(inputElement).toBeTruthy();
  });

  it('button should exist', () => {
    const inputElement = fixture.debugElement.nativeElement.querySelector('button');
    expect(inputElement).toBeTruthy();
  });
});
