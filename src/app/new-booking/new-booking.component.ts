import {Component, OnInit} from '@angular/core';
import {NlfEntriesService} from '../services/nlf-entries.service';
import {NLFEntry} from '../app.component';
import {NgForm} from '@angular/forms';
import {MatSnackBar} from '@angular/material';
import {Router} from '@angular/router';

@Component({
  selector: 'app-new-booking',
  templateUrl: './new-booking.component.html',
  styleUrls: ['./new-booking.component.scss']
})
export class NewBookingComponent implements OnInit {
  private entries: NLFEntry[];
  borrowerName: string;
  days: string;
  bookName: any;
  isbn: any;

  constructor(private nlfEntriesService: NlfEntriesService,
              private snackBar: MatSnackBar,
              private router: Router) {
  }

  ngOnInit() {
    this.nlfEntriesService.currentEntries.subscribe(nlfEntries => this.entries = nlfEntries);
  }

  addBookingEntry(form: NgForm) {
    if (form.valid) {
      this.entries.push({
        id: (this.entries.length + 1).toString(),
        name: this.borrowerName,
        days: this.days,
        bookName: this.bookName,
        isbn: this.isbn
      });
      this.snackBar.open(this.borrowerName + ' is successfully registered!', '', {
        duration: 3000,
        panelClass: 'center',
        verticalPosition: 'top',
        horizontalPosition: 'right'
      });
      this.router.navigateByUrl('/bookings');
    }
  }
}
