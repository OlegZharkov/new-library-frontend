import {Component, OnInit} from '@angular/core';
import {NlfEntriesService} from './services/nlf-entries.service';

export interface NLFEntry {
  id: string;
  name: string;
  days: string;
  bookName: string;
  isbn: number;
}

const BOOKS: string[] = [
  'Shabaz Benjamin', 'Precious Knights', 'Bailey Hook', 'Manal Buchanan', 'Harrison Hodge', 'Agatha Norton', 'Tara Reilly',
  'Kashif Jackson', 'Fredrick Mohammed', 'Alysha Mclean', 'Kaden Boyd', 'Genevieve Hammond', 'Isaak Valenzuela', 'Victoria Melia',
  'Willow Barlow', 'Imani Mackenzie', 'Enya Plummer', 'Darcy Forrest', 'Dawn Mellor', 'Ava Chamberlain', 'Willard Golden',
  'Ksawery Roberts', 'Yaseen Deleon', 'Hilda Nunez', 'Mathew Tyler'
];

const NAMES: string[] = [
  'Easter', 'Mikki', 'Jarred', 'Elene', 'Ozie', 'Susanna', 'Belle', 'Gus', 'Ilda', 'Rae', 'Thomasena', 'Julianna', 'Ali', 'Erline', 'Avery',
  'Mavis', 'Ardelle', 'Roberta', 'Leonora', 'Barney'
];

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'new-library-frontend';
  nlfEntries: NLFEntry[];

  constructor(private nlfEntriesService: NlfEntriesService) {
    // check if values were already generated!
    const retrievedObject = localStorage.getItem('nlfEntries');
    if (retrievedObject !== null && retrievedObject !== undefined) {
      this.nlfEntries = JSON.parse(retrievedObject);
    } else {
      // Create 100 entries
      this.nlfEntries = Array.from({length: 100}, (_, k) => createNewRandomLoanEntry(k + 1));
      localStorage.setItem('nlfEntries', JSON.stringify(this.nlfEntries));
    }
    this.nlfEntriesService.updateEntries(this.nlfEntries);
  }

  ngOnInit() {
    this.nlfEntriesService.currentEntries.subscribe(nlfEntries => this.nlfEntries = nlfEntries);
  }

}

/** Builds and returns a new User. */
function createNewRandomLoanEntry(id: number): NLFEntry {
  const name = NAMES[Math.round(Math.random() * (NAMES.length - 1))] + ' ' +
    NAMES[Math.round(Math.random() * (NAMES.length - 1))].charAt(0) + '.';

  return {
    id: id.toString(),
    name,
    days: Math.round(Math.random() * 30).toString(),
    bookName: BOOKS[Math.round(Math.random() * (BOOKS.length - 1))],
    // please note that ISBN is absolutely imaginary and doesn't meet real ISBN standards.
    isbn: Math.floor(100000000 + Math.random() * 900000000)
  };
}
