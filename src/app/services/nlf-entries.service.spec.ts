import {inject, TestBed} from '@angular/core/testing';

import {NlfEntriesService} from './nlf-entries.service';

describe('NlfEntriesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NlfEntriesService = TestBed.get(NlfEntriesService);
    expect(service).toBeTruthy();
  });

  it('should provide entries', inject([NlfEntriesService], (service: NlfEntriesService) => {
    expect(service.currentEntries).toBeTruthy();
  }));
});
