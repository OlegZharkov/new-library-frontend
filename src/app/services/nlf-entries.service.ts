import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import {NLFEntry} from '../app.component';

@Injectable({
  providedIn: 'root'
})
export class NlfEntriesService {

  private entriesSource: BehaviorSubject<NLFEntry[]> = new BehaviorSubject([]);
  currentEntries = this.entriesSource.asObservable();

  constructor() { }

  updateEntries(message: NLFEntry[]) {
    this.entriesSource.next(message);
  }

}
