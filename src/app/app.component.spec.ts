import {TestBed, async, inject} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {AppComponent} from './app.component';
import {MaterialModule} from './material-module';
import {NlfEntriesService} from './services/nlf-entries.service';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        MaterialModule
      ],
      declarations: [
        AppComponent
      ]
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'new-library-frontend'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('new-library-frontend');
  });

  it(`should create random nlfEntries`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    // initialize component and check localStorage for nlfEntries
    expect(localStorage.getItem('nlfEntries')).toBeTruthy();
  });

  it('mat-toolbar-row should exist', () => {
    const tableElement = TestBed.createComponent(AppComponent).debugElement.nativeElement.querySelector('mat-toolbar-row');
    expect(tableElement).toBeTruthy();
  });

  it('router link should exist', () => {
    const tableElement = TestBed.createComponent(AppComponent).debugElement.nativeElement.querySelector('a');
    expect(tableElement).toBeTruthy();
  });
});
