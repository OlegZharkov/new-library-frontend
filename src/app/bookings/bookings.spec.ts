import {async, ComponentFixture, inject, TestBed} from '@angular/core/testing';
import {MaterialModule} from '../material-module';
import {BookingsComponent} from './bookings.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {By} from 'protractor';


describe('BookingsComponent', () => {
  let component: BookingsComponent;
  let fixture: ComponentFixture<BookingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [MaterialModule, BrowserAnimationsModule],
      declarations: [BookingsComponent],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('table should exist', () => {
    const tableElement = fixture.debugElement.nativeElement.querySelector('table');
    expect(tableElement).toBeTruthy();
  });

});
