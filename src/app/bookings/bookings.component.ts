import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTable, MatTableDataSource} from '@angular/material/table';
import {NLFEntry} from '../app.component';
import {ActivatedRoute, Router} from '@angular/router';
import {NlfEntriesService} from '../services/nlf-entries.service';


@Component({
  selector: 'app-bookings',
  templateUrl: './bookings.component.html',
  styleUrls: ['./bookings.component.scss']
})
export class BookingsComponent implements OnInit {
  // columns
  displayedColumns: string[] = ['id', 'name', 'days', 'bookName', 'isbn', 'actions'];
  dataSource: MatTableDataSource<NLFEntry>;
  entries: NLFEntry[];
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private nlfEntriesService: NlfEntriesService) {}

  ngOnInit() {
    this.nlfEntriesService.currentEntries.subscribe(nlfEntries => this.entries = nlfEntries);
    this.dataSource = new MatTableDataSource(this.entries);
    this.sortTable();
  }

  sortTable() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  deleteLoan(id: string) {
    this.entries = this.entries.filter(entry => entry.id !== id);
    this.nlfEntriesService.updateEntries(this.entries);
    this.dataSource = new MatTableDataSource(this.entries);
    this.sortTable();
  }
}
