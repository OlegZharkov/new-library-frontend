import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {BookingsComponent} from './bookings/bookings.component';
import {HomeComponent} from './home/home.component';
import {NewBookingComponent} from './new-booking/new-booking.component';


const routes: Routes = [
  {path: 'bookings', component: BookingsComponent},
  {path: 'home', component: HomeComponent},
  {path: 'new-booking', component: NewBookingComponent},
  {path: '', redirectTo: 'home', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
